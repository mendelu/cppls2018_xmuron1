TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    programator.cpp \
    pracovni_pozice.cpp \
    manazer.cpp \
    firma.cpp

HEADERS += \
    programator.h \
    pracovni_pozice.h \
    manazer.h \
    firma.h
