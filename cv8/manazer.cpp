#include "manazer.h"

Manazer::Manazer(std::string jmeno, std::string oddeleni):PracovniPozice(jmeno){
    m_oddeleni = oddeleni;
}

float Manazer::getPlat(){
    return 50000+m_pocetLetNaPozici*1000;
}

std::string Manazer::getOddeleni(){
    return m_oddeleni;
}
