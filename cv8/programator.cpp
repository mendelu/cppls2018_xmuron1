#include "programator.h"

Programator::Programator(std::string jmeno,std::string hlavniJazyk):PracovniPozice(jmeno){
    m_hlavniJazyk = hlavniJazyk;
}

float Programator::getPlat(){
    return 30000+m_pocetLetNaPozici*1000;
}

std::string Programator::getHlavniJazyk(){
    return m_hlavniJazyk;
}
