#include <vector>
#include "pracovni_pozice.h"

#ifndef FIRMA_H
#define FIRMA_H


class Firma
{
private:
std::vector<PracovniPozice*> m_pracovni_pozice;
public:
    void pridejPracovniPozici(PracovniPozice* pozice);
    int kolikMeStojiZamestnanci();
};

#endif // FIRMA_H
