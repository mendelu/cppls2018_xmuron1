#include <iostream>
#include "firma.h"
#include "programator.h"
#include "manazer.h"

/*
 * Mejme firmu, ve firme je mozno pracovat na dvou pracovnich pozicich - programator a manazer.
 * U vsech zamestancu je evidovano jmeno pracovni pozice a pocet let na pozici.
 * Vsichni maji metodu getPlat().
 * U programatora se pocita jako 30000+pocetLet*1000, u manazera jako 50000+pocetLet*1000.
 * U programatora dale evidujeme jeho hlavni programovaci jazyk
 * U manazera evidujeme ktere oddeleni firmy ma na starost
 */
using namespace std;


int main () {
    Firma* moje_firma = new Firma();
    Programator* hlavni_progamator = new Programator("Hlavni programator","C++");
    Manazer* manazer_uctarny = new Manazer("Manazer uctarny","Uctarna");

    moje_firma->pridejPracovniPozici(hlavni_progamator);
    moje_firma->pridejPracovniPozici(manazer_uctarny);
    cout << "Zamestnanci me stoji: " << moje_firma->kolikMeStojiZamestnanci() << endl;

    hlavni_progamator->setPocetLetNaPozici(10);
    cout << hlavni_progamator->getPlat() << endl;
    cout << manazer_uctarny->getPlat() << endl;


}
