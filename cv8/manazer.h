#include "pracovni_pozice.h"

#ifndef MANAZER_H
#define MANAZER_H


class Manazer:public PracovniPozice{
protected:
    std::string m_oddeleni;
public:
    Manazer(std::string jmeno, std::string oddeleni);
    float getPlat();
    std::string getOddeleni();
};

#endif // MANAZER_H
