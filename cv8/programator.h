#include "pracovni_pozice.h"

#ifndef PROGRAMATOR_H
#define PROGRAMATOR_H


class Programator:public PracovniPozice{
protected:
    std::string m_hlavniJazyk;
public:
    Programator(std::string jmeno, std::string hlavniJazyk);
    float getPlat();
    std::string getHlavniJazyk();
};


#endif // PROGRAMATOR_H
