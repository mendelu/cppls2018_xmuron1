#include "firma.h"

void Firma::pridejPracovniPozici(PracovniPozice* pozice){
    m_pracovni_pozice.push_back(pozice);
}
int Firma::kolikMeStojiZamestnanci(){
    int sum = 0;
    for(int i=0;i<m_pracovni_pozice.size();i++){
        PracovniPozice* pozice = m_pracovni_pozice.at(i);
        sum += pozice->getPlat();
    }
    return sum;
}
