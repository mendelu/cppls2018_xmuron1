#include <iostream>

#ifndef ZAMESTNANEC_H
#define ZAMESTNANEC_H

class PracovniPozice{
protected:
    std::string m_jmeno;
    int m_pocetLetNaPozici;
public:
    PracovniPozice(std::string jmeno);
    virtual float getPlat() = 0;
    void setPocetLetNaPozici(int pocetLet);
};


#endif // ZAMESTNANEC_H
