TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Brneni.cpp \
    Helma.cpp \
    KrouzkoveBrneni.cpp \
    LehkaUtocnaHelma.cpp \
    LehkoOdenecBuilder.cpp \
    PlatoveBrneni.cpp \
    Rytir.cpp \
    RytirBuilder.cpp \
    RytirDirector.cpp \
    TezkaUtocnaHelma.cpp \
    TezkoOdenecBuilder.cpp

HEADERS += \
    Brneni.h \
    Helma.h \
    KrouzkoveBrneni.h \
    LehkaUtocnaHelma.h \
    LehkoOdenecBuilder.h \
    PlatoveBrneni.h \
    Rytir.h \
    RytirBuilder.h \
    RytirDirector.h \
    TezkaUtocnaHelma.h \
    TezkoOdenecBuilder.h
