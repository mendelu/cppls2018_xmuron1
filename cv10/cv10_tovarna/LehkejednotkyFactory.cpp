#include "LehkejednotkyFactory.h"

rytiri::Rytir* rytiri::LehkejednotkyFactory::getRytir(string jmeno, int sila) {
	return new LehkoOdenec(jmeno,sila,m_vahaHelmy);
}

rytiri::LehkejednotkyFactory::LehkejednotkyFactory(int vahaHelmyLehkoodence) {
	m_vahaHelmy = vahaHelmyLehkoodence;
}
