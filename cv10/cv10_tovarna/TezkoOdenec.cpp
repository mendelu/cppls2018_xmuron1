#include "TezkoOdenec.h"

rytiri::TezkoOdenec::TezkoOdenec(string jmeno, int sila, int silaZbroje):Rytir(jmeno,sila) {
	m_silaZbroje = silaZbroje;
}

int rytiri::TezkoOdenec::getUtok() {
	return getSila() + m_silaZbroje;
}

int rytiri::TezkoOdenec::getObrana() {
	return m_silaZbroje;
}

void rytiri::TezkoOdenec::setSilaZbroje(int silaZbroje) {
	m_silaZbroje = silaZbroje;
}
