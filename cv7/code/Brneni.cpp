#include <exception>
using namespace std;

#include "Brneni.h"
Brneni::Brneni(int pocatecni_bonus, int vaha) {
    m_bonus = pocatecni_bonus;
    m_vaha = vaha;
}

int Brneni::getBonus() {
    return m_bonus;
}

int Brneni::getVaha() {
    return m_vaha;
}

