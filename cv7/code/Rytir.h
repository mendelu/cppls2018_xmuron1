#include <exception>
#include <vector>
#include <iostream>
using namespace std;

#ifndef __Rytir_h__
#define __Rytir_h__

// #include "Brneni.h"
// #include "Zbran.h"
#include "Napoj.h"

class Brneni;
class Zbran;
class Napoj;
class Rytir;

class Rytir
{
	private: int m_obrana;
	private: int m_utok;
	private: Brneni* m_brneni;
	private: int m_zivot;
	private: Zbran* m_zbran;
	private: std::vector<Napoj*> m_napoje;

	public: Rytir(int pocatecni_obrana, int pocatecni_utok, int pocatecni_zivot);

	public: int getObrana();

	public: int getUtok();

	public: void seberBrneni(Brneni* brneni);

	public: void seberZbran(Zbran* zbran);

	public: void seberNapoj(Napoj* napoj);

	public: void zahodZbran();

	public: void zahodBrneni();

	public: void vypijPosledniNapoj();

	public: void vypijNapoj(int pozice_napoje);

	public: void printInfo();
};

#endif
