#include <iostream>
#include "Rytir.h"
#include "Zbran.h"
#include "Brneni.h"

using namespace std;

int main()
{
    Rytir* artus = new Rytir(5,10,100);
    artus->printInfo();

    // seberu mec
    Zbran* mec = new Zbran(10);
    artus->seberZbran(mec);
    artus->printInfo();

    // seberu brneni
    Brneni* platove = new Brneni(5,2);
    artus->seberBrneni(platove);
    artus->printInfo();

    // zkusim vypit napoj, ale zatim zadny nemam
    artus->vypijPosledniNapoj();

    // seberu tri napoje a vypiju je, kazdy jinou metodou
    Napoj* limonada = new Napoj(10);
    Napoj* pivo = new Napoj(-5);
    Napoj* kava = new Napoj(20);

    artus->seberNapoj(limonada);
    artus->seberNapoj(pivo);
    artus->seberNapoj(kava);

    artus->printInfo();
    // zkusim vypit napoj na pozici 100, zadny tam neni
    artus->vypijNapoj(100);

    // vypiju napoj na druhe pozici (pocitame od nuly)
    artus->vypijNapoj(1);

    // vypiju napodledy pridany napoj
    artus->vypijPosledniNapoj();

    artus->printInfo();



    return 0;
}
