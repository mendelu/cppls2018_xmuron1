#include <exception>
using namespace std;

#ifndef __Napoj_h__
#define __Napoj_h__

class Napoj;

class Napoj
{
	private: int m_bonus;

	public: Napoj(int sila);

	public: int getBonus();
};

#endif
