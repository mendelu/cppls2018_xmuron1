#include <exception>
using namespace std;

#ifndef __Brneni_h__
#define __Brneni_h__

class Brneni;

class Brneni
{
	private: int m_bonus;
	private: int m_vaha;

	public: Brneni(int pocatecni_bonus, int vaha);

	public: int getBonus();

	public: int getVaha();
};

#endif
