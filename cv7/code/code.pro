TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Brneni.cpp \
    Napoj.cpp \
    Rytir.cpp \
    Zbran.cpp

HEADERS += \
    Brneni.h \
    Napoj.h \
    Rytir.h \
    Zbran.h
