#include <exception>
using namespace std;

#ifndef __Zbran_h__
#define __Zbran_h__

class Zbran;

class Zbran
{
	private: int m_bonus;

	public: Zbran(int pocatecni_bonus);

	public: int getBonus();
};

#endif
