#include <exception>
#include <vector>
using namespace std;

#include "Rytir.h"
#include "Brneni.h"
#include "Zbran.h"
#include "Napoj.h"

Rytir::Rytir(int pocatecni_obrana, int pocatecni_utok, int pocatecni_zivot) {
    m_obrana = pocatecni_obrana;
    m_utok = pocatecni_utok;
    m_zivot = pocatecni_zivot;
    m_zbran = nullptr;
    m_brneni = nullptr;
}

int Rytir::getObrana() {
    // kontrola, zda na sobe mam brneni
    if(m_brneni != nullptr){
        return m_obrana + m_brneni->getBonus();
    }
    else{
        return m_obrana;
    }
}

int Rytir::getUtok() {
    // kontrola, zda mam zbran
    if(m_zbran != nullptr){
        return m_utok + m_zbran->getBonus();
    }
    else{
        return m_utok;
    }
}

void Rytir::seberBrneni(Brneni* brneni) {
    m_brneni = brneni;
}

void Rytir::seberZbran(Zbran* zbran) {
    m_zbran = zbran;
}

void Rytir::seberNapoj(Napoj* napoj) {
    // pridam napoj do vektoru
    m_napoje.push_back(napoj);
}

void Rytir::zahodZbran() {
    m_zbran = nullptr;
}

void Rytir::zahodBrneni() {
    m_brneni = nullptr;
}

void Rytir::vypijPosledniNapoj() {
    // kontrola zda mam nejake napoje
    if(! m_napoje.empty() ){
        // ziskam ukazatel na posledni napoj ve vektoru
        Napoj* napoj = m_napoje.back();
        // z napoje ziskam bonus a prictu k zivotum
        m_zivot += napoj->getBonus();
        // odstranim napoj z vektoru - uz jsem ho vypil.
        m_napoje.pop_back();
    }
    else{
        cout << "Nemas zadne napoje." << endl;
    }
}

void Rytir::vypijNapoj(int pozice_napoje) {
    // zkontroluju, zda je zadana pozice v rozmezi
    if(pozice_napoje < m_napoje.size() && pozice_napoje >= 0){
        // prictu bonus napoje k zivotum
        m_zivot += m_napoje.at(pozice_napoje)->getBonus();
        // smazu napoj z vektoru na zadane pozici
        m_napoje.erase(m_napoje.begin() + pozice_napoje);
    }
    else{
        cout << "Na zadane pozici neni zadny napoj." << endl;
    }
}

void Rytir::printInfo() {
    cout << "Zivoty: " << m_zivot << endl;
    cout << "Utok: " << this->getUtok()  << endl;
    cout << "Obrana: " << this->getObrana()  << endl;
    cout << "Pocet napoju: " << m_napoje.size()  << endl;
}

