#ifndef NAPOJ_H
#define NAPOJ_H

class Napoj {

private:
	int m_bonus;

public:
	Napoj(int sila);

	int getBonus();
};

#endif
