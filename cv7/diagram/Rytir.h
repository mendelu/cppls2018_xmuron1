#ifndef RYTIR_H
#define RYTIR_H

class Rytir {

private:
	int m_obrana;
	int m_utok;
	Brneni* m_brneni;
	int m_zivot;
	Zbran* m_zbran;
	std::vector<Napoj> m_napoje;

public:
	Rytir(int pocatecni_obrana, int pocatecni_utok, int pocatecni_zivot);

	int getObrana();

	int getUtok();

	void seberBrneni(Brneni* brneni);

	void seberZbran(Zbran* zbran);

	void seberNapoj(Napoj* napoj);

	void zahodZbran();

	void zahodBrneni();

	void vypijPosledniNapoj();

	void vypijNapoj(int pozice_napoje);

	void printInfo();
};

#endif
