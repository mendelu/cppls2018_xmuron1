#ifndef BRNENI_H
#define BRNENI_H

class Brneni {

private:
	int m_bonus;
	int m_vaha;

public:
	Brneni(int pocatecni_bonus, int vaha);

	int getBonus();

	int getVaha();
};

#endif
