#include <iostream>
#include <vector>

using namespace std;

class Napoj{
private:
    string m_jmeno;
public:
    Napoj(string jmeno){
        m_jmeno = jmeno;
    }
    string getJmeno(){
        return m_jmeno;
    }
};


int main()
{
    // vytvorim vektor
    vector<Napoj*> muj_vektor;

    Napoj* napoj_1 = new Napoj("pivo");
    Napoj* napoj_2 = new Napoj("kava");
    Napoj* napoj_3 = new Napoj("limonada");

    // pridam napoje do vektoru
    muj_vektor.push_back(napoj_1);
    muj_vektor.push_back(napoj_2);
    muj_vektor.push_back(napoj_3);

    cout << "Jmeno napoje na druhe pozici at(1): " << muj_vektor.at(1)->getJmeno() << endl;
    cout << "Velikost vektoru: " << muj_vektor.size() << endl;

    // odeberu napoj od konce
    muj_vektor.pop_back();
    cout << "Velikost vektoru: " << muj_vektor.size() << endl;

    // vypisu posledni napoj z vectoru
    Napoj* n = muj_vektor.back();
    cout << "Naposledy pridany: " << n->getJmeno() << endl;

    // co se stane?
    // muj_vektor.at(100);

    return 0;
}
