#include <iostream>

using namespace std;

class Motorka{
public:
    string m_spz;
    int m_vykon;
    float m_km;

    void printInfo(){
        cout<<"SPZ motorky je: "<<m_spz<<endl;
        cout<<"Vykon: "<<m_vykon<<endl;
        cout<<"Najeto km: "<<m_km<<endl;
    }

    int getKm(){
        return m_km;
    }

    void setKm(float km){
        if(km > 0){
            m_km = km;
        }
    }

};

int main()
{
    Motorka* motorka = new Motorka();

    //motorka->printInfo();

    motorka->setKm(10000);

    cout<<motorka->getKm()<<endl;

    delete motorka;
    return 0;
}
