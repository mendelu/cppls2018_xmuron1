#ifndef PRIKAZUPIJTROCHU_H
#define PRIKAZUPIJTROCHU_H
#include <iostream>
#include "Prikaz.h"

using namespace std;
class PrikazUpijTrochu : public Prikaz {


public:
	void PouzijLektvar(Lektvar* lektvar, Hrdina* hrdina);

	string getPopis();
};

#endif // PRIKAZUPIJTROCHU_H
