#include "PrikazVypijVse.h"

void PrikazVypijVse::PouzijLektvar(Lektvar* lektvar, Hrdina* hrdina) {
    int kolikBonus = lektvar->getBonusZivot();
    hrdina->setZivot(kolikBonus);
    lektvar->setBonusZivot(0);
    cout<<"Vypil jsi lektvar, zivot se zvysil na: "<<hrdina->getZivot()<<endl;
}

string PrikazVypijVse::getPopis() {
    return "vypit cely lektvar";
}
