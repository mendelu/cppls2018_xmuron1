#include "Engine.h"

Engine::Engine() {
    cout<<"Instance vytvorena"<<endl;
}

Engine* Engine::getInstance() {
    if(s_engine == nullptr){
        s_engine = new Engine();
    } else {
        cout<<"uz je vytvorena instance"<<endl;
    }

    return s_engine;
}

void Engine::addPlaneta(string jaka) {
    m_planety.push_back(jaka);
}

void Engine::vypisInfo() {
    for(int i = 0; i < m_planety.size(); i++){
        cout<<"Na pozici: "<<i<<" je ulozena planeta: "<<m_planety.at(i)<<endl;
    }
}
