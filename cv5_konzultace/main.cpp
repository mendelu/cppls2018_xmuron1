#include <iostream>

using namespace std;

/*
Namodelujte třídu Zvířátko, které je potravou pro draka.
Každé zvířátko má váhu a kalorickou hodnotu, které je potřeba znát již při vytváření.
Zvířátko je potravou pro draka.
Každý drak si eviduje hmotnost sežrané potravy a kalorickou hodnotu již sežrané potravy.
Drak musí implementovat metodu Sežer(), která přenastavuje celkovou hmotnost a kalorickou hodnotu již sežrané potravy.
Potřebujete-li pro zadání nějaké další metody, pak si je zaveďte.
*/

class Zviratko{
private:
    int m_vaha;
    int m_kalorie;

public:
    Zviratko(int vaha, int kalorie){
        m_vaha = vaha;
        m_kalorie = kalorie;
    }

    int getVaha(){
     return m_vaha;
    }

    int getKalorie(){
        return m_kalorie;
    }

};

class Dracek{
private:
    int m_sezranaVaha;
    int m_sezraneKalorie;
public:
    Dracek(){
        m_sezranaVaha = 0;
        m_sezraneKalorie = 0;
    }

    Dracek(int pocatecniVaha, int pocatecniKalorie){
        m_sezranaVaha = pocatecniVaha;
        m_sezraneKalorie = pocatecniKalorie;
    }

    void printInfo(){
        cout << "Celkova snezena vaha: " << m_sezranaVaha << endl;
        cout << "Celkove snezene kalorie: " << m_sezraneKalorie << endl;
    }

    void sezer(Zviratko* snezene_zvirato){
        m_sezranaVaha += snezene_zvirato->getVaha();
        m_sezraneKalorie += snezene_zvirato->getKalorie();
    }

    void sezer(int vaha, int kalorie){
        m_sezranaVaha += vaha;
        m_sezraneKalorie += kalorie;
    }
};

int main()
{
    Dracek* mujDrak = new Dracek();
    Zviratko* zajic = new Zviratko(10,20);
    Zviratko* slon = new Zviratko(200,100);


    mujDrak->printInfo();

    mujDrak->sezer(zajic);
    mujDrak->sezer(slon->getVaha(),slon->getKalorie());

    mujDrak->printInfo();

    delete mujDrak;
    delete zajic;
    delete slon;
    return 0;
}
