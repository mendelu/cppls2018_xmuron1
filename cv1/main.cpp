#include <iostream>

using namespace std;

bool muzuPit(int vek)
{
    if(vek < 18){
        return false;
    }
    else{
        return true;
    }
}

int main()
{
    string jmeno;
    string text_let = "Kolik ti je?";
    int vek = 0;
    cout << "Rekni jak se jmenujes" << endl;
    cin >> jmeno;
    cout << "Tvoje jmeno je: " << jmeno << endl;

    cout << text_let << endl;
    cin >> vek;
    cout << "Mas " << vek << " let" << endl;

    if(muzuPit(vek)){
        cout << "Tady mas" << endl;
    }
    else {
        cout << "Nedostanes pivo" << endl;
    }
    return 0;
}
