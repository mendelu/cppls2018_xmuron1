#include <iostream>

using namespace std;

class Motorka{
public:
    string m_spz;
    float m_vykon;
    float m_km;

    ~Motorka(){
        cout<<"Konec motorky s SPZ: "<<m_spz<<endl;
    }

    Motorka(){
        m_spz = "";
        m_vykon = 0;
        m_km = 0;
    }

    Motorka(string spz, float vykon, float km){
        m_spz = spz;
        m_vykon = vykon;
        m_km = km;
    }

    void printInfo(){
        cout<<"SPZ motorky je: "<<m_spz<<endl;
        cout<<"Vykon: "<<m_vykon<<endl;
        cout<<"Najeto km: "<<m_km<<endl;
    }

    string getSPZ(){
        return m_spz;
    }

    void setSPZ(string spz){
        if(spz.size() == 8){
            m_spz = spz;
        }
    }

    int getVykon(){
        return m_vykon;
    }

    void setVykon(int vykon){
        if(vykon > 0) {
            m_vykon = vykon;
        }
    }

    float getKm(){
        return m_km;
    }

    void setKm(float km){
        if(km > 0){
            m_km = km;
        }
    }

};

int main()
{
    //Motorka* motorka = new Motorka();

    Motorka* motorka = new Motorka("3Bsdfgsdgdf39698", -1000, 20000);
    motorka->setVykon(-100);
    motorka->printInfo();
    delete motorka;
    return 0;
}
