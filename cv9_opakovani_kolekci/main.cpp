#include <iostream>
#include <vector>

class Prvek{
private:
    std::string m_jmeno;
public:
    Prvek(std::string jmeno){
      m_jmeno = jmeno;
    }

    std::string getJmeno(){
        return m_jmeno;
    }
};

int main()
{
    std::vector<Prvek*> moje_kolekce;
    Prvek* prvek1 = new Prvek("slon");
    Prvek* prvek2 = new Prvek("mamut");

    // pridam prvek
    moje_kolekce.push_back(prvek1);
    moje_kolekce.push_back(prvek2);

    // odeberu posledni
    // moje_kolekce.pop_back();

    // vypisu
    std::cout << "Posledni pridany prvek: "
              << moje_kolekce.back()->getJmeno()
              << std::endl;
    std::cout << "Prvni pridany prvek: "
              << moje_kolekce.front()->getJmeno()
              << std::endl;

    // vypis vsechny prvky
    std::cout << "Vypisuju vsechny prvky: " << std::endl;
    for(int i=0;i<moje_kolekce.size();i++){
        std::cout << moje_kolekce.at(i)->getJmeno() << std::endl;
    }


    return 0;
}
