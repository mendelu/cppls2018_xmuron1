#include <iostream>

using namespace std;

class Motorka{
public:
    string m_spz;
    int m_vykon;
    float m_km;

    Motorka(string spz, int vykon, float km){
       setSPZ(spz);
       setVykon(vykon);
       setKm(km);
    }

    Motorka(){
        m_spz = "";
        m_vykon = 0;
        m_km = 0;
    }

    ~Motorka(){
        cout << "Konec teto motorky" << endl;
    }

    void printInfo(){
        cout<<"SPZ motorky je: "<<m_spz<<endl;
        cout<<"Vykon: "<<m_vykon<<endl;
        cout<<"Najeto km: "<<m_km<<endl;
    }

    string getSPZ(){
        return m_spz;
    }

    void setSPZ(string spz){
        if(spz.size() == 7){
            m_spz = spz;
        }
        else{
            m_spz = 'nic';
        }
    }

    int getVykon(){
        return m_vykon;
    }

    void setVykon(int vykon){
        if(vykon > 0) {
            m_vykon = vykon;
        }
        else{
            m_vykon = 0;
        }
    }

    int getKm(){
        return m_km;
    }

    void setKm(float km){
        if(km > 0){
            m_km = km;
        }
        else{
            m_km = 0;
        }
    }

};

int main()
{
    Motorka* motorka = new Motorka("1234567", -1000, -20000);
    motorka->printInfo();
    delete motorka;

    /*
    jina_motorka->printInfo();

    motorka->setVykon(800);
    motorka->setSPZ("4B5 9568");
    motorka->setKm(10000);

    motorka->printInfo();

    delete motorka;
    delete jina_motorka;
    */

    return 0;
}
