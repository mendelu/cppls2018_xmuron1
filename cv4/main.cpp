#include <iostream>

using namespace std;

class Trida{
public:
    static int s_pocet;

    Trida(){
        Trida::s_pocet += 1;
    }

    ~Trida(){
        Trida::s_pocet -= 1;
    }

    void objektVypisPocetInstanci(){
        cout << "Pocet instanci je:" << Trida::s_pocet << endl;
    }

    static void tridaVypisPocetInstanci(){
        cout << "Pocet instanci je:" << s_pocet << endl;
    }

};

int Trida::s_pocet = 0;

int main()
{
    Trida* t1 = new Trida();
    Trida* t2 = new Trida();
    t1->objektVypisPocetInstanci();
    t1->tridaVypisPocetInstanci();
    delete t1;
    Trida::tridaVypisPocetInstanci();
    return 0;
}
