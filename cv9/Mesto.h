#ifndef __Mesto_h__
#define __Mesto_h__

#include <iostream>
#include <string>
#include <vector>
#include "Budova.h"

class Budova;
class Mesto;

class Mesto
{
protected:
    std::string m_jmeno;
    std::vector<Budova*> m_budovy;

public:
    Mesto(std::string jmeno);
    void pridejBudovu(Budova* budova);
    void odstranPosledniBudovu();
    int getPocetObyvatel();
    int getPocetObyvatelNaPozici(int pozice);
    void vypisVsechnyBudovy();
};

#endif
