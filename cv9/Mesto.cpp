#include "Mesto.h"

Mesto::Mesto(std::string jmeno) {
    m_jmeno = jmeno;
}

void Mesto::pridejBudovu(Budova* budova) {
    m_budovy.push_back(budova);
}

void Mesto::odstranPosledniBudovu() {
    m_budovy.pop_back();
}

int Mesto::getPocetObyvatel() {
    int sum = 0;
    for(int i=0;i<m_budovy.size();i++){
        sum += m_budovy.at(i)->getPocetObyvatel();
    }
    return sum;
}

int Mesto::getPocetObyvatelNaPozici(int pozice){
    // potreba pridat kontrolu predaneho parametru pozice
    Budova* budova = m_budovy.at(pozice);
    return budova->getPocetObyvatel();
}

void Mesto::vypisVsechnyBudovy(){
    for(Budova* budova : m_budovy){
        std::cout << budova->getPocetObyvatel() << " : " << budova->getVyska() << std::endl;
    }
}
