#ifndef __Budova_h__
#define __Budova_h__

class Mesto;
class Budova;

class Budova
{
    private:
    int m_pocetObyvatel;
    int m_vyska;

    public:
    Budova(int pocetObyvatel, int vyska);
    int getPocetObyvatel();
    int getVyska();

};

#endif
