#ifndef __Velkomesto_h__
#define __Velkomesto_h__

#include "Mesto.h"

// class Mesto;
class Velkomesto;

class Velkomesto: public Mesto
{
private:
    std::string m_jmeno_agromerace;
public:
    Velkomesto(std::string jmeno_mesta, std::string jmeno_agromerace);
    std::string getJmenoAgromerace();
};

#endif
