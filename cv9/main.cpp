#include <iostream>
#include "Velkomesto.h"

using namespace std;

int main()
{
    Velkomesto* aglomerace = new Velkomesto("Praha mesto","Kraj Praha");
    Budova* b1 = new Budova(100,1);
    Budova* b2 = new Budova(200,2);

    aglomerace->pridejBudovu(b1);
    aglomerace->pridejBudovu(b2);

    int pocetNaPozici = aglomerace->getPocetObyvatelNaPozici(0);
    int pocetObyvatelCelkem = aglomerace->getPocetObyvatel();

    cout << pocetObyvatelCelkem << endl;

    aglomerace->vypisVsechnyBudovy();

    return 0;
}
