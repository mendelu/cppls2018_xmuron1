TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Budova.cpp \
    Mesto.cpp \
    Velkomesto.cpp

HEADERS += \
    Budova.h \
    Mesto.h \
    Velkomesto.h
