#include "Velkomesto.h"


Velkomesto::Velkomesto(std::string jmeno_mesta, std::string jmeno_agromerace): Mesto (jmeno_mesta){
  m_jmeno_agromerace = jmeno_agromerace;
}


std::string Velkomesto::getJmenoAgromerace(){
    return m_jmeno_agromerace;
}
