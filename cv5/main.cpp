#include <iostream>

using namespace std;

class Nastaveni{
private:
    static float m_obtiznost;
public:
    static void setObtiznost(float obtiznost){
       m_obtiznost = obtiznost;
    }

    static float getObtiznost(){
        return m_obtiznost;
    }
};


class Napoj{
private:
    int m_bonus;
public:
    Napoj(int bonus){
        m_bonus = bonus;
    }

    int getBonus(){
       return m_bonus;
    }
};

class Hrdina{
private:
    int m_zdravi;
    static int m_velikost_druziny;
public:
    Hrdina(int zdravi){
        m_zdravi = zdravi;
        Hrdina::m_velikost_druziny += 1;
    }

    ~Hrdina(){
        Hrdina::m_velikost_druziny -= 1;
    }

    static int getVelikostDruziny(){
        return Hrdina::m_velikost_druziny;
    }

    int getZdravi(){
        return m_zdravi;
    }

    void vypijNapoj(int bonus){
        m_zdravi += bonus*Nastaveni::getObtiznost();
    }

    void vypijNapoj(Napoj* napoj){
        m_zdravi += napoj->getBonus()*Nastaveni::getObtiznost();
    }

};

int Hrdina::m_velikost_druziny = 0;
float Nastaveni::m_obtiznost = 0;

int main()
{

    Nastaveni::setObtiznost(0.5);
    Hrdina* artus = new Hrdina(100);
    Napoj* caj = new Napoj(10);
    Napoj* kava = new Napoj(20);


    cout<<artus->getZdravi()<<endl;
    artus->vypijNapoj(caj->getBonus());
    cout<<artus->getZdravi()<<endl;
    artus->vypijNapoj(kava);
    cout<<artus->getZdravi()<<endl;

    delete artus;
    delete caj;
    delete kava;
    return 0;
}
